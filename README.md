# sisop-praktikum-modul-4-2023-BJ-U05

## Group U05

Abiansyah Adzani Gymnastiar (5025211077)
I Putu Arya Prawira Wiwekananda (5025211065)
Satria Surya Prana (5025211073)

## Soal 1
1. Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.



Catatan: 
Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
Perhatikan port  pada masing-masing instance.

- code untuk penyelesaian nomor 1
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>


void parseCSV(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Failed to open the CSV file.\n");
        return;
    }

    char line[1000][1000][100];
    int row_count = 0;

    while (fgets(line[row_count][0], sizeof(line[row_count][0]), file)) {
        char *token = strtok(line[row_count][0], ",");
        int col_count = 0;

        while (token) {
            strcpy(line[row_count][col_count], token);
            token = strtok(NULL, ",");
            col_count++;
        }

        row_count++;
    }

    fclose(file);

    // Print player information based on criteria
    printf("Player Information:\n");
    for (int i = 0; i < row_count; i++) {
        char *name = line[i][1];
        char *club = line[i][6];
        int age = atoi(line[i][3]);
        int potential = atoi(line[i][13]);
        char *photo_url = line[i][17];

        if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0) {
            printf("Name: %s\n", name);
            printf("Club: %s\n", club);
            printf("Age: %d\n", age);
            printf("Potential: %d\n", potential);
            printf("Photo URL: %s\n", photo_url);
            printf("\n");
        }
    }
}


void extract(char namafile[]){
if (fork()==0){
   char *argv[]={"unzip", namafile, NULL};
   execv("/usr/bin/unzip", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

int main() {
    
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    
    extract("fifa-player-stats-database.zip");

    const char *filename = "FIFA23_official_data.csv";
    parseCSV(filename);

    return 0;
}

```

## Soal 2
1. Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt
Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:
Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut.

### Pengerjaan dan Pembahasan
- code untuk Fungsi-fungsi FUSE
``` C
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    res = lstat(path, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char full_path[10001];
        snprintf(full_path, sizeof(full_path), "%s/%s", path, de->d_name);

        memset(&st, 0, sizeof(st));

        if (lstat(full_path, &st) == -1)
            continue;

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void)fi;

    fd = open(path, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    // Check if the source or destination path contains "restricted"
    if (strstr(path, "restricted") != NULL)
    {
        writeLog("FAILED", "MKDIR", "Operation not permitted");
        return -EPERM; // Operation not permitted
    }

    int res;

    res = mkdir(path, mode);

    if (res == -1)
    {
        writeLog("FAILED", "MKDIR", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "MKDIR", path);

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    // Check if the source or destination path contains "restricted"
    if (strstr(from, "restricted") != NULL || strstr(to, "restricted") != NULL)
    {
        // Check if it's a directory and the destination name has "bypass"
        struct stat stbuf;
        int res = lstat(from, &stbuf);
        if (res == -1)
        {
            writeLog("FAILED", "RENAME", strerror(errno));
            return -errno;
        }

        if (S_ISDIR(stbuf.st_mode) && strstr(to, "bypass") != NULL)
        {
            // Perform the rename operation
            res = rename(from, to);
            if (res == -1)
            {
                writeLog("FAILED", "RENAME", strerror(errno));
                return -errno;
            }

            writeLog("SUCCESS", "RENAME", from);

            return 0;
        }
        else
        {
            writeLog("FAILED", "RENAME", "Operation not permitted");
            return -EPERM; // Operation not permitted
        }
    }

    int res = rename(from, to);
    if (res == -1)
    {
        writeLog("FAILED", "RENAME", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "RENAME", from);

    return 0;
}

static int xmp_unlink(const char *path)
{
    // Check if the file path contains "restricted"
    if (strstr(path, "restricted") != NULL)
    {
        writeLog("FAILED", "RMFILE", "Operation not permitted");
        return -EPERM; // Operation not permitted
    }

    int res = unlink(path);
    if (res == -1)
    {
        writeLog("FAILED", "RMFILE", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "RMFILE", path);

    return 0;
}

static int xmp_rmdir(const char *path)
{
    // Check if the directory path contains "restricted"
    if (strstr(path, "restricted") != NULL)
    {
        writeLog("FAILED", "RMDIR", "Operation not permitted");
        return -EPERM; // Operation not permitted
    }

    int res = rmdir(path);
    if (res == -1)
    {
        writeLog("FAILED", "RMDIR", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "RMDIR", path);

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,   // Retrieves the attributes of a file or directory
    .readdir = xmp_readdir,   // Reads the contents of a directory
    .read = xmp_read,         // Reads data from a file
    .mkdir = xmp_mkdir,       // Creates a new directory
    .rename = xmp_rename,     // Renames a file or directory
    .unlink = xmp_unlink,     // Deletes a file
    .rmdir = xmp_rmdir,       // Deletes a directory
};
```
- code untuk main
``` C
int main(int argc, char *argv[])
{
    umask(0);

    // Download the ZIP file
    downloadZipFile();

    // Extract the ZIP file
    extractZipFile();

    // Create the log file
    FILE *logFile = fopen(LOG_FILE_PATH, "w");
    if (logFile != NULL)
    {
        fclose(logFile);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
- Code ini akan membuat FUSE dimana folder and file memiliki attribute yang sesuai dengan soal.
- semua aktivitas yang dilakukan dalam fuse akan tercatat dalam logmucatatsini.txt

## Soal 3
1. Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c

Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

```c
#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static int isEncodeRequired(const char *path)
{
    const char *dirName = strrchr(path, '/') + 1;  // Mendapatkan nama direktori atau file dari path
    char firstChar = dirName[0];  // Mengambil huruf pertama dari nama direktori atau file

    // Mengembalikan 1 (true) jika huruf pertama adalah L, U, T, atau H
    if (firstChar == 'L' || firstChar == 'U' || firstChar == 'T' || firstChar == 'H')
        return 1;
    else
        return 0;
}

static char *base64Encode(const char *input, int length)
{
    BIO *bio, *b64;
    FILE *stream;
    int encodedLength = 4 * ((length + 2) / 3);
    char *output = (char *)malloc(encodedLength + 1);

    stream = fmemopen(output, encodedLength + 1, "w");
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(stream, BIO_NOCLOSE);
    BIO_push(b64, bio);
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO_write(b64, input, length);
    BIO_flush(b64);
    BIO_free_all(b64);
    fclose(stream);

    return output;
}

static int secretadmirer_getattr(const char *path, struct stat *stbuf)
{
    char full_path[100];
    sprintf(full_path, "/etc%s", path);
    int res = lstat(full_path, stbuf);
    if (res == -1)
        return -errno;

    // Jika direktori atau file perlu di-encode
    if (isEncodeRequired(path)) {
        // Mendapatkan nama direktori atau file dari path
        const char *dirName = strrchr(path, '/') + 1;

        // Meng-encode nama direktori atau file dengan Base64
        char *encodedName = base64Encode(dirName, strlen(dirName));

        // Mengganti nama direktori atau file dengan versi ter-encode
        char encodedPath[100];
        sprintf(encodedPath, "%s%s", "/etc", encodedName);
        strcpy(stbuf->st_name, encodedPath);

        free(encodedName);
    }

    return 0;
}

// Implementasi fungsi-fungsi FUSE lainnya (opsional) sesuai kebutuhan Dhafin.

static struct fuse_operations secretadmirer_oper = {
    .getattr = secretadmirer_getattr,
    // Daftar fungsi-fungsi FUSE lainnya yang ingin Dhafin implementasikan.
};

int main(int argc, char *argv[])
{
    return fuse_main(argc, argv, &secretadmirer_oper, NULL);
}
```
