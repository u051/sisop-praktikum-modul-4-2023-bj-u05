#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>

#define DOWNLOAD_PATH "folder3" // Set the desired download folder path here
#define LOG_FILE_PATH "logmucatatsini.txt" // Set the path where the log file will be stored

static const char *ZIP_FILE_PATH = "folder3/file.zip"; // Set the path where the downloaded ZIP file will be stored

// Function to download the ZIP file using wget
static void downloadZipFile()
{
    // Construct the wget command
    char command[256];
    snprintf(command, sizeof(command), "wget -O %s 'https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i'", ZIP_FILE_PATH);

    // Execute the wget command using system()
    system(command);
}

// Function to unzip
static void extractZipFile()
{
    // Construct the unzip command
    char command[256];
    snprintf(command, sizeof(command), "unzip %s -d %s", ZIP_FILE_PATH, DOWNLOAD_PATH);

    // Execute the unzip command using system()
    system(command);
}

// Function to write logs
static void writeLog(const char *status, const char *command, const char *desc)
{
    time_t t;
    struct tm *tm_info;
    char buffer[30];

    time(&t);
    tm_info = localtime(&t);

    strftime(buffer, sizeof(buffer), "%d/%m/%Y-%H:%M:%S", tm_info);

    FILE *logFile = fopen(LOG_FILE_PATH, "a");
    if (logFile != NULL)
    {
        fprintf(logFile, "%s::%s::%s::%s\n", status, buffer, command, desc);
        fclose(logFile);
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    res = lstat(path, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char full_path[10001];
        snprintf(full_path, sizeof(full_path), "%s/%s", path, de->d_name);

        memset(&st, 0, sizeof(st));

        if (lstat(full_path, &st) == -1)
            continue;

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void)fi;

    fd = open(path, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    // Check if the source or destination path contains "restricted"
    if (strstr(path, "restricted") != NULL)
    {
        writeLog("FAILED", "MKDIR", "Operation not permitted");
        return -EPERM; // Operation not permitted
    }

    int res;

    res = mkdir(path, mode);

    if (res == -1)
    {
        writeLog("FAILED", "MKDIR", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "MKDIR", path);

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    // Check if the source or destination path contains "restricted"
    if (strstr(from, "restricted") != NULL || strstr(to, "restricted") != NULL)
    {
        // Check if it's a directory and the destination name has "bypass"
        struct stat stbuf;
        int res = lstat(from, &stbuf);
        if (res == -1)
        {
            writeLog("FAILED", "RENAME", strerror(errno));
            return -errno;
        }

        if (S_ISDIR(stbuf.st_mode) && strstr(to, "bypass") != NULL)
        {
            // Perform the rename operation
            res = rename(from, to);
            if (res == -1)
            {
                writeLog("FAILED", "RENAME", strerror(errno));
                return -errno;
            }

            writeLog("SUCCESS", "RENAME", from);

            return 0;
        }
        else
        {
            writeLog("FAILED", "RENAME", "Operation not permitted");
            return -EPERM; // Operation not permitted
        }
    }

    int res = rename(from, to);
    if (res == -1)
    {
        writeLog("FAILED", "RENAME", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "RENAME", from);

    return 0;
}

static int xmp_unlink(const char *path)
{
    // Check if the file path contains "restricted"
    if (strstr(path, "restricted") != NULL)
    {
        writeLog("FAILED", "RMFILE", "Operation not permitted");
        return -EPERM; // Operation not permitted
    }

    int res = unlink(path);
    if (res == -1)
    {
        writeLog("FAILED", "RMFILE", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "RMFILE", path);

    return 0;
}

static int xmp_rmdir(const char *path)
{
    // Check if the directory path contains "restricted"
    if (strstr(path, "restricted") != NULL)
    {
        writeLog("FAILED", "RMDIR", "Operation not permitted");
        return -EPERM; // Operation not permitted
    }

    int res = rmdir(path);
    if (res == -1)
    {
        writeLog("FAILED", "RMDIR", strerror(errno));
        return -errno;
    }

    writeLog("SUCCESS", "RMDIR", path);

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,   // Retrieves the attributes of a file or directory
    .readdir = xmp_readdir,   // Reads the contents of a directory
    .read = xmp_read,         // Reads data from a file
    .mkdir = xmp_mkdir,       // Creates a new directory
    .rename = xmp_rename,     // Renames a file or directory
    .unlink = xmp_unlink,     // Deletes a file
    .rmdir = xmp_rmdir,       // Deletes a directory
};

int main(int argc, char *argv[])
{
    umask(0);

    // Download the ZIP file
    downloadZipFile();

    // Extract the ZIP file
    extractZipFile();

    // Create the log file
    FILE *logFile = fopen(LOG_FILE_PATH, "w");
    if (logFile != NULL)
    {
        fclose(logFile);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
