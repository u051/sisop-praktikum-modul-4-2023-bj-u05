#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>


void parseCSV(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Failed to open the CSV file.\n");
        return;
    }

    char line[1000][1000][100];
    int row_count = 0;

    while (fgets(line[row_count][0], sizeof(line[row_count][0]), file)) {
        char *token = strtok(line[row_count][0], ",");
        int col_count = 0;

        while (token) {
            strcpy(line[row_count][col_count], token);
            token = strtok(NULL, ",");
            col_count++;
        }

        row_count++;
    }

    fclose(file);

    // Print player information based on criteria
    printf("Player Information:\n");
    for (int i = 0; i < row_count; i++) {
        char *name = line[i][1];
        char *club = line[i][6];
        int age = atoi(line[i][3]);
        int potential = atoi(line[i][13]);
        char *photo_url = line[i][17];

        if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0) {
            printf("Name: %s\n", name);
            printf("Club: %s\n", club);
            printf("Age: %d\n", age);
            printf("Potential: %d\n", potential);
            printf("Photo URL: %s\n", photo_url);
            printf("\n");
        }
    }
}


void extract(char namafile[]){
if (fork()==0){
   char *argv[]={"unzip", namafile, NULL};
   execv("/usr/bin/unzip", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

int main() {
    
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    
    extract("fifa-player-stats-database.zip");

    const char *filename = "FIFA23_official_data.csv";
    parseCSV(filename);

    return 0;
}

